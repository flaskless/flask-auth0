from app import app as flask_app
import pytest


@pytest.fixture(scope='session')
def app():
    flask_app.testing = True
    flask_app.logger.info("Testing Flask App")
    return flask_app

from app.blueprints.auth.models import Auth0User

VerifiedUser = Auth0User(dict(email_verified=True))
UnverifiedUser = Auth0User(dict(email_verified=False))

@pytest.fixture(scope='function')
def logged_in_user(app):
    @app.login_manager.request_loader
    def load_user_from_request(u):
        return VerifiedUser

@pytest.fixture(scope='function')
def logged_out_user(app):
    @app.login_manager.request_loader
    def load_user_from_request(u):
        return UnverifiedUser