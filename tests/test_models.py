from flask_auth0.models import Auth0User as User
import flask_login

class TestUser(object):
    def test_user_has_no_subscription(self,logged_in_user):
        assert flask_login.current_user is not None

    def _test_subscribed_user_receives_more_coins(self, users):
        """ Subscribed user receives more coins. """
        user = User.find_by_identity('admin@local.host')

        assert user.coins == 210
