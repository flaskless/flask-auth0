from flask import url_for
import flask_login


class TestLogin(object):
    def test_login_page(self,client,logged_out_user):
        """ Login page renders successfully. """
        response = client.get(url_for('auth.login'))
        assert response.status_code == 302

        response = client.get(url_for('calculator.ui'))
        assert response.status_code == 302 

        response = client.get(url_for('pages.home'))
        assert response.status_code == 200

    def test_not_logged_in(self,client,logged_out_user):
        import flask_login
        assert flask_login.current_user.is_anonymous == False
        assert flask_login.current_user.is_authenticated == False
        resp = client.get('/auth/dashboard')
        assert resp.status_code == 302

    def test_logged_in(self,client,logged_in_user):
        """ Login successfully. """
        resp = client.get('/auth/dashboard')
        assert resp.status_code == 200
        assert flask_login.current_user.is_anonymous == False
        assert flask_login.current_user.is_authenticated == True


#class TestSettings(object):
#    def test_settings_page(self, client, logged_in_user):
#        """ Settings renders successfully. """
#
#        response = client.get(url_for('users.settings'))
#        assert response.status_code == 200
#
#class TestUpdateLocale(object):
#    def test_update_locale_page(self, client, logged_in_user):
#        """ Update locale renders successfully. """
#        self.login()
#        response = client.get(url_for('users.update_locale'))
#        assert response.status_code == 200
#
#    def test_locale(self, users):
#        """ Locale works successfully. """
#        user = {'locale': 'kl'}
#        response = self.client.post(url_for('users.update_locale'), data=user,
#                                    follow_redirects=True)
#
#        assert_status_with_message(200, response,
#                                   'Your locale settings have been updated.')
#
#    def test_klingon_locale(self, users):
#        """ Klingon locale works successfully. """
#        user = User.find_by_identity('admin@local.host')
#        user.locale = 'kl'
#        user.save()
#
#        self.login()
#
#        response = self.client.get(url_for('billing.purchase_coins'))
#
#        # Klingon for "Card".
#        assert_status_with_message(200, response, 'Chaw')
#