
from flask import Flask, jsonify

from flask_auth0 import Auth0

app = Flask(__name__
            ,instance_relative_config=True)

app.config.from_pyfile('settings.py')

app.secret_key = 'secret'

app.config.setdefault('AUTH0_LOGOUT_URL', 'http://localhost:5000/logout')
app.config.setdefault('AUTH0_CALLBACK_URL', 'http://localhost:5000/callback')
app.config.setdefault('AUTH0_DOMAIN', '<your company>.eu.auth0.com')
app.config.setdefault('AUTH0_CLIENT_ID', 'YOUR_AUTH0_CLIENT_ID')
app.config.setdefault('AUTH0_CLIENT_SECRET', 'YOUR_AUTH0_CLIENT_SECRET')

auth0 = Auth0(app)

@app.route('/')
@auth0.requires_auth
def hello():
    return 'Hello world!'

@app.route('/logout')
@auth0.requires_auth
def logout():
    auth0.logout()
    return jsonify('logout')


if __name__ == '__main__':
    app.run()