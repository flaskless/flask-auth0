"""Python Flask WebApp Auth0 integration example
"""

# NOT USED

from functools import wraps
import json
from werkzeug.exceptions import HTTPException

from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode

class constants:
    """ Constants file for Auth0's seed project
    """
    AUTH0_CLIENT_ID = 'AUTH0_CLIENT_ID'
    AUTH0_CLIENT_SECRET = 'AUTH0_CLIENT_SECRET'
    AUTH0_CALLBACK_URL = 'AUTH0_CALLBACK_URL'
    AUTH0_DOMAIN = 'AUTH0_DOMAIN'
    AUTH0_AUDIENCE = 'AUTH0_AUDIENCE'
    PROFILE_KEY = 'profile'
    JWT_PAYLOAD = 'jwt_payload'


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if constants.PROFILE_KEY not in session:
            return redirect('/login')
        return f(*args, **kwargs)

    return decorated

def init_app(app):

    oauth = OAuth(app)

    AUTH0_CALLBACK_URL = app.config.get(constants.AUTH0_CALLBACK_URL)
    AUTH0_CLIENT_ID = app.config.get(constants.AUTH0_CLIENT_ID)
    AUTH0_CLIENT_SECRET = app.config.get(constants.AUTH0_CLIENT_SECRET)
    AUTH0_DOMAIN = app.config.get(constants.AUTH0_DOMAIN)
    AUTH0_BASE_URL = 'https://' + AUTH0_DOMAIN
   # AUTH0_AUDIENCE = app.config.get(constants.AUTH0_AUDIENCE)


    auth0 = oauth.register(
        'auth0',
        client_id=AUTH0_CLIENT_ID,
        client_secret=AUTH0_CLIENT_SECRET,
        api_base_url=AUTH0_BASE_URL,
        access_token_url=AUTH0_BASE_URL + '/oauth/token',
        authorize_url=AUTH0_BASE_URL + '/authorize',
        client_kwargs={
            'scope': 'openid profile email',
        },
    )

    # Controllers API
    @app.route('/')
    def home():
        return render_template('auth/home.html')
#
    #@app.errorhandler(Exception)
    #def handle_auth_error(ex):
    #    response = jsonify(message=str(ex))
    #    response.status_code = (ex.code if isinstance(ex, HTTPException) else 500)
    #    return response

    @app.route('/callback')
    def callback_handling():
        auth0.authorize_access_token()
        resp = auth0.get('userinfo')
        userinfo = resp.json()

        session[constants.JWT_PAYLOAD] = userinfo
        session[constants.PROFILE_KEY] = {
            'user_id': userinfo['sub'],
            'name': userinfo['name'],
            'picture': userinfo['picture']
        }
        return redirect('/dashboard')

    @app.route('/login')
    def login():
        return auth0.authorize_redirect(
                redirect_uri=AUTH0_CALLBACK_URL, 
                #audience=AUTH0_AUDIENCE
                )


    @app.route('/logout')
    def logout():
        session.clear()
        params = {'returnTo': url_for('home', _external=True), 'client_id': AUTH0_CLIENT_ID}
        return redirect(auth0.api_base_url + '/v2/logout?' + urlencode(params))


    @app.route('/dashboard')
    @requires_auth
    def dashboard():
        return render_template('auth/dashboard.html',
                               userinfo=session[constants.PROFILE_KEY],
                               userinfo_pretty=json.dumps(session[constants.JWT_PAYLOAD], indent=4))