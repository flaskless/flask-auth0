import flask_login
from pydantic import BaseModel
from enum import Enum
from . import flask_auth0
auth0 = flask_auth0.Auth0()


class Role(Enum):
    """Different roles available to users.  Multiple roles are possible"""
    admin = 'admin'
    member = 'member'

class UserMetadata(BaseModel):
    """Data that could be updated by the user"""
    address: str = None
    locale: str = None

class Subscription(BaseModel):
    """Details about the user's subscription"""
    plan: str
    coupon: str

class AppMetadata(BaseModel):
    """Data that should never be updated directly by the user"""
    stripe_id: str = None
    subcription: Subscription = None

class UserDetails(BaseModel):
    """User Profile Data from Auth0"""
    email: str = None
    user_id: str = None
    email_verified: bool = False
    name: str = None
    username: str = None
    picture: str = None
    
    user_metadata: UserMetadata = UserMetadata()
    app_metadata: AppMetadata = AppMetadata()

class Auth0User(flask_login.UserMixin):

    def __init__(self,details: dict):
        self._details: UserDetails = UserDetails(**details)

    @classmethod
    def find_by_id(cls,uid):
        auth0_manager = auth0.client()
        u = auth0_manager.users.get(uid)
        return cls(u)

    @property
    def is_active(self):
        """Users will only be logged in if this returns true"""

        return True

    @property
    def is_authenticated(self):
        return True

    @property
    def is_anonymous(self):
        return False

    @property
    def id(self):
        return self._details.user_id

    @property
    def locale(self):
        l = self._details.user_metadata.locale
        return l

    @locale.setter
    def locale(self,l):
        self._details.user_metadata.locale = l

    def get_id(self):
        return str(self.id)

    
    def has_role(self, role: Role) -> bool:
        roles = auth0.client().users.list_roles(self.id)['roles']
        for role in roles:
            if role['name'] == role.value:
                return True 
        return False

    def save(self):
        readonly = {'name','picture','email','email_verified','user_id'}

        updated_user = self._details.dict(exclude_none=True,exclude=readonly)

        response = auth0.client().users.update(
            self.id,
            body=updated_user
        )
        return response 


    @property
    def userinfo(self) -> dict:
        if self._details.user_metadata:
            return self._details.user_metadata.dict()
        return {}
    
    @property
    def email(self):
        return self._details.email

    @property
    def name(self):
        return self._details.name

    @property
    def picture(self):
        return self._details.picture

    @property
    def subscription(self):
        return self._details.app_metadata.subcription