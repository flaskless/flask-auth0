# Created by Samuel Lesuffleur 29-09-2017
#
# Copyright (c) 2017 Sandtable Ltd. All rights reserved.
"""
    Flask-Auth0 is an extension for Flask that allows you to authenticate
    through Auth0 service.
"""
import json
import logging
from functools import wraps
import time
from urllib.parse import urlparse, urlencode
from authlib.integrations.flask_client import OAuth
import flask
from flask import current_app

logger = logging.getLogger(__name__)


class AuthError(Exception):
    def __init__(self, error, status_code=401):
        self.error = error
        self.status_code = status_code



SILENT_AUTH_ERROR_CODES = ('login_required',
                           'interaction_required',
                           'consent_required')


class Auth0(object):
    """The core Auth0 client object."""

    def __init__(self, app=None):
        self._server = None

        # A Callback to 
        self._on_authenticated = None
        """A function that will be called after user is authenticated """

        # TODO: Add config validation
        self._auth0_management_client = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app: flask.Flask):
        """Do setup required by a Flask app."""
        app.config.setdefault('AUTH0_ALGORITHMS', 'RS256')
        app.config.setdefault('AUTH0_AUDIENCE', None)
        app.config.setdefault('AUTH0_CALLBACK_URL','/auth0/callback')
        app.config.setdefault('AUTH0_CLIENT_ID', None)
        app.config.setdefault('AUTH0_CLIENT_SECRET', None)
        app.config.setdefault('AUTH0_DOMAIN', None)
        app.config.setdefault('AUTH0_TENANT', app.config['AUTH0_DOMAIN'])
        app.config.setdefault('AUTH0_LOGOUT_URL','/')
        app.config.setdefault('AUTH0_REQUIRE_VERIFIED_EMAIL', True)
        app.config.setdefault('AUTH0_SESSION_JWT_PAYLOAD_KEY', 'jwt_payload')
        app.config.setdefault('AUTH0_SESSION_TOKEN_KEY', 'auth0_token')
        app.config.setdefault('AUTH0_SCOPE', 'openid profile email')
        app.config.setdefault('AUTH0_ENABLE_SILENT_AUTHENTICATION', False)
        app.config.setdefault('AUTH0_URL_PREFIX', '/auth0')
        app.config.setdefault('AUTH0_CALLBACK_ENDPOINT_NAME','auth0.callback')

        self._auth0_tenant = app.config['AUTH0_TENANT']
        self._client_id = app.config['AUTH0_CLIENT_ID']
        self._client_secret = app.config['AUTH0_CLIENT_SECRET']
        self._domain = app.config['AUTH0_DOMAIN']
        self._base_url = 'https://{}'.format(self._domain)
        self._access_token_url = self._base_url + '/oauth/token'
        self._callback_url = app.config.get('AUTH0_CALLBACK_URL')
        self._logout_url = app.config.get('AUTH0_LOGOUT_URL')
        self._authorize_url = self._base_url + '/authorize'
        default_audience = self._base_url + '/userinfo'
        self._audience = app.config.get('AUTH0_AUDIENCE', default_audience)
        self._scope = app.config['AUTH0_SCOPE']
        self._session_token_key = app.config['AUTH0_SESSION_TOKEN_KEY']
        self._session_jwt_payload_key = \
            app.config['AUTH0_SESSION_JWT_PAYLOAD_KEY']
        self._silent_auth_enabled = \
            app.config['AUTH0_ENABLE_SILENT_AUTHENTICATION']
        self._callback_endpoint = app.config['AUTH0_CALLBACK_ENDPOINT_NAME']

        def fetch_token(name):
            return flask.session.get(f"{name}_token")

        auth0 = OAuth(app,fetch_token=fetch_token)
        self._auth0 = auth0.register(
            'auth0',
            client_id=self._client_id,
            client_secret=self._client_secret,
            api_base_url=self._base_url,
            access_token_url=self._access_token_url,
            authorize_url=self._authorize_url,
            client_kwargs={
                'audience': self._audience,
                'scope': self._scope,
            },
        )

        if self._callback_url.startswith('/'):
            route = urlparse(self._callback_url).path
            app.route(rule=route,endpoint=self._callback_endpoint)(self._callback)

        @app.errorhandler(AuthError)
        def handle_auth_error(ex):
            print(f'Caught error in auth0 extenion: {ex}')
            response = flask.jsonify(ex.error)
            response.status_code = ex.status_code
            return response

    def _callback(self):
        """Redirect to the originally requested page."""
        args = flask.request.args.to_dict(flat=True)
        try:
            state = json.loads(args['state'])
            destination = state['destination']
            silent_auth = state.get('silent_auth')
        except (ValueError, KeyError) as e:
            logger.exception(e)
            raise AuthError('Invalid callback request')

        token = self._auth0.authorize_access_token()
        if not token:
            error_code = args['error']
            error_desc = args['error_description']

            if silent_auth is True and error_code in SILENT_AUTH_ERROR_CODES:
                logger.warning("Silent authentication failed: '{}'".format(
                    error_code))
                self._redirect_to_auth_server(destination, silent=False)

            message = "Authentication failed: '{}' (code='{}')".format(
                error_desc, error_code)
            raise AuthError(message, 401)

        resp = self._auth0.get('userinfo')
        userinfo = resp.json()
        flask.session[self._session_token_key] = token
        flask.session[self._session_jwt_payload_key] = userinfo


        try:
            uid = userinfo['sub']
            u = self.client().users.get(uid)
            if self._on_authenticated:
                logger.debug('calling on_authenticate callback on `{}`'.format(u))
                self._on_authenticated(u)
        except Exception as e:
            logger.exception(e)
            raise AuthError(f'Authentication Failed!: {e}')

        parsed = urlparse(destination)
        if parsed.scheme:
            # its a external URL, redirect to it
            next_url = destination
        elif parsed.path.startswith('/'):
            # its an absolute url, redirect to it
            next_url = destination
        else:
            # its a flask endpoint, generate the external url for it
            scheme = current_app.config['PREFERRED_URL_SCHEME']
            next_url = flask.url_for(destination, _external=True, _scheme=scheme)
        logger.debug('redirecting to `{}`'.format(next_url))
        return flask.redirect(next_url)

    def _redirect_to_auth_server(self, destination, silent=True):
        """Redirect to the auth0 server.

        Args:
           destination: URL to redirect user
           silent: do silent authentication?
        """
        state = {
            'destination': destination,
        }
        params = {}

        if silent is True and self._silent_auth_enabled:
            state['silent_auth'] = True
            params['prompt'] = 'none'
        
        return self._auth0.authorize_redirect(
            redirect_uri=self.redirect_uri,
            audience=self._audience,
            state=json.dumps(state),
            **params
        )

    def requires_auth(self, view_func):
        """Decorates view functions that require a user to be logged in."""
        @wraps(view_func)
        def decorated(*args, **kwargs):
            if not self.access_token:
                logger.debug('user requires authentication')
                return self._redirect_to_auth_server(flask.request.endpoint)
            if int(time.time()) >= int(self.access_token['expires_at']):
                logger.debug('token expired')
                return self._redirect_to_auth_server(flask.request.endpoint)
            logger.debug('user is authenticated')
            return view_func(*args, **kwargs)
        return decorated

    def logout(self,return_to=None):
        """Logout user.

        Request the browser to clear the current session and also clear the
        Auth0 session (clearing the SSO cookie).

        The user won't be log out from a third party Identity Provider.

        The user is redirected to the logout callback.
        """
        flask.session.clear()
        if return_to is None:
            return_to = self._logout_url if not self._logout_url.startswith('/') else flask.request.host_url + self._logout_url.lstrip('/')
        params = {'returnTo': return_to, 'client_id': self._client_id}
        url = self._auth0.api_base_url + '/v2/logout?' + urlencode(params)
        logger.debug('redirecting to `{}`'.format(url))
        return flask.redirect(url)

    @property
    def access_token(self):
        """Get access token (user must be authenticated)."""
        tokens = flask.session.get(self._session_token_key)
        if not tokens:
            return None
        return tokens

    @property
    def jwt_payload(self):
        """Get JWT payload (user must be authenticated)."""
        return flask.session.get(self._session_jwt_payload_key, None)

    @property
    def redirect_uri(self):
        return flask.url_for(self._callback_endpoint,_external=True)

    def login(self, destination):
        """Redirect to the auth0 server.

        Args:
           destination: URL to redirect user
        """

        state = json.dumps({"destination": destination})
        return self._auth0.authorize_redirect(
            redirect_uri=self.redirect_uri,
            audience=self._audience,
            state=state)

    def on_authenticated(self, callback):
        '''
        This will set the callback that is invoked after successful login from the server.

        The callback must accept the user object as the only argument, and should store the
        user info in the session.

        :param callback: The callback after authenticating users.
        :type callback: callable
        '''
        self._on_authenticated = callback
        return callback

    def client(self):
        """ Access the Auth0 Management API """

        if not self._auth0_management_client:
            from auth0.v3.authentication import GetToken
            non_interactive_client_id = self._client_id
            non_interactive_client_secret = self._client_secret

            get_token = GetToken(self._domain)
            token = get_token.client_credentials(non_interactive_client_id,
                non_interactive_client_secret, 'https://{}/api/v2/'.format(self._domain))

            mgmt_api_token = token['access_token']

            from auth0.v3.management import Auth0 as Auth0Client

            self._auth0_management_client = Auth0Client(self._domain, mgmt_api_token)
        
        return self._auth0_management_client