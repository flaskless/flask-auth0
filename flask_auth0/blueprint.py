from flask import Blueprint, render_template, session, current_app
import json
import flask_login
from .models import Auth0User, auth0

auth = Blueprint('auth', __name__,
         template_folder='templates'
         ,static_url_path='/static'
         ,static_folder='static'
         ,url_prefix='/auth')


@auth.route('/login')
def login():
    return auth0.login(destination='/calculator')

@auth.route('/logout')
@flask_login.login_required
def logout():
    flask_login.logout_user()
    return auth0.logout()

@auth.route('/dashboard')
@flask_login.login_required
def dashboard():
    userinfo = flask_login.current_user.userinfo
    return render_template('dashboard.html',
                           userinfo_pretty=json.dumps(userinfo, indent=4))

# Automatically called after being registered with the app the first time.
@auth.record_once
def init_app(state):
    """
    Initialize the Flask-Login extension (mutates the app passed in).
    """

    # Use existing login_manager if possible, other create one.
    login_manager = state.app.extensions.get('login')

    if not login_manager:
        state.app.logger.debug(f"""[{__name__}.{auth.name}] \n
        Creating a new 'flask_login.LoginManager' because one was 
        not found in app.extenions["login"].
        """)

        login_manager = flask_login.LoginManager(app=state.app)
        state.app.extensions['login'] = login_manager

    # Get the auth0 configuration from the app
    auth0.init_app(app=state.app)
    
    # send user to auth.login if not authenticated
    login_manager.login_view = 'auth.login'

    # define the function that gets the User object
    @login_manager.user_loader
    def load_user(uid):
        return Auth0User.find_by_id(uid)
     
    # define the function that logs-in the user after Auth0 verification
    def login_auth0_user(user):
        """ Store the user in the cookie session """
        u = Auth0User(user)
        if False and current_app.config.get("STRIPE_SECRET") and not u._details.app_metadata.stripe_id:
            current_app.logger.info(f'Creating Stripe Customer for user {u.email}')
            u._details.app_metadata.stripe_id = create_stripe_customer(u)
            u.save()
            
        return flask_login.login_user(u)

    auth0.on_authenticated(login_auth0_user)

    def create_stripe_customer(user: Auth0User):
        import stripe

        params = {
            'email': user.email,
            'metadata': dict(auth0_user_id=user.id),
            'name' : user.name
        }
        stripe.api_key = current_app.config['STRIPE_SECRET']
        customer = stripe.Customer.create(**params)
        return customer['id']