from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

INSTALL_REQUIRES=[
# Authentication
"Flask",
"authlib>=0.15",
"auth0-python>=3.13.0",
# Requests as needed for Authentication
"requests>=2.25.1",
"chardet",
#"chardet>=4.0.0",
"idna>=2.10",
]
setup(name='flask-auth0',
      version='0.1.0dev0',
      description='Flask Auth0',
      long_description=long_description,
      long_description_content_type="text/markdown",
      packages=['flask_auth0'],
      install_requires=INSTALL_REQUIRES,
    #  python_requires='>=3.6',
      )
